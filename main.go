package main

import (
	"gitlab.com/brandonpinto/simplus-todo/router"
)

func main() {
	// Initialize Router
	router.InitRouter()
}
