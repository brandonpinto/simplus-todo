package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func ListTodos(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"msg": "GET todo",
	})
}

func CreateTodo(ctx *gin.Context) {
	ctx.JSON(http.StatusNoContent, gin.H{
		"msg": "POST todo",
	})
}

func UpdateTodo(ctx *gin.Context) {
	ctx.JSON(http.StatusNoContent, gin.H{
		"msg": "PUT todo",
	})
}

func DeleteTodo(ctx *gin.Context) {
	ctx.JSON(http.StatusNoContent, gin.H{
		"msg": "DELETE todo",
	})
}
