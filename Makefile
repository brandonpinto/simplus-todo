build:
	@go build -o bin/simplus-todo

run: build
	@./bin/simplus-todo

test:
	@go test -v ./...

up:
	docker-compose up -d

down:
	docker-compose down
