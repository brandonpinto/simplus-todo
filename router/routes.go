package router

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/brandonpinto/simplus-todo/handler"
)

func initRoutes(router *gin.Engine) {
	v1 := router.Group("/api/v1")
	{
		v1.GET("/todo", handler.ListTodos)
		v1.POST("/todo", handler.CreateTodo)
		v1.PUT("/todo", handler.UpdateTodo)
		v1.DELETE("/todo", handler.DeleteTodo)
	}
}
