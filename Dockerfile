FROM golang:1.20-alpine3.18

WORKDIR /usr/src/app

COPY . .

EXPOSE 3000

RUN go mod tidy

RUN go build -o ./bin/simplus-todo

CMD [ "./bin/simplus-todo" ]
